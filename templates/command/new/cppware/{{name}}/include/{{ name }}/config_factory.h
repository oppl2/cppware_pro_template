#ifndef {{ name|upper }}_CONFIG_FACTORY_H_
#define {{ name|upper }}_CONFIG_FACTORY_H_

#include "{{name}}/config.h"

namespace cppware {
namespace {{name}} {


class ConfigFactory {
 public:
  static Config::ConfigPtr instance();

 protected:
  ConfigFactory(ConfigFactory&) = delete;
  ConfigFactory& operator=(const ConfigFactory&) = delete;
  static Config::ConfigPtr instance_;
};

}  // namespace {{name}}
}  // namespace cppware



#endif  // {{ name|upper }}_CONFIG_FACTORY_H_
