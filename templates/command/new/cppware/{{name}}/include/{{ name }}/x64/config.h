/*
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 20:46:16
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-17 22:42:50
 */
#ifndef {{ name|upper }}_X64_CONFIG_H_
#define {{ name|upper }}_X64_CONFIG_H_

#include <memory>
#include <string>
#include "{{name}}/config.h"

#define D_SRVMANAGER_CONFIG_FILE    "/usr/framework/etc/{{name}}/srvmanager.json"
// process resource
#define MAX_CPU_USED_PERPROCESS_DEFAULT   40            // percent
#define MAX_MEM_USED_PERPROCESS_DEFAULT   (200*1024)    // 200M
#define MAX_MEM_FREE_THRESHOLD_DEFAULT    (150*1024)    // 150M

// sdf file
#define DEFAULT_SDF_PROP "/usr/framework/etc/{{name}}/sdf/prop.default"
#define SERVICE_MODE_FILE "/usr/framework/etc/{{name}}/mode.conf"

// root directory
#define FRAMEWORK_ROOT_DIR  "/usr/framework"

namespace cppware {
namespace {{name}} {

class Configx64 : public Config {
 public:
  using Configx64Ptr = std::shared_ptr<Configx64>;
  explicit Configx64(const std::string &confile);

 protected:
  Configx64(Configx64&) = delete;
  Configx64& operator=(const Configx64&) = delete;
  void default_fill();
};

}  // namespace {{name}}
}  // namespace cppware

#endif  // {{ name|upper }}_X64_CONFIG_H_
