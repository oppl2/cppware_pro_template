/*
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 20:46:16
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-17 22:40:06
 */
#ifndef {{ name|upper }}_AARCH64_CONFIG_H_
#define {{ name|upper }}_AARCH64_CONFIG_H_

#include <string>
#include <memory>
#include "{{name}}/config.h"

#define D_SRVMANAGER_CONFIG_FILE    "/framework/etc/{{name}}/srvmanager.json"
// process resource
#define MAX_CPU_USED_PERPROCESS_DEFAULT   40            // percent
#define MAX_MEM_USED_PERPROCESS_DEFAULT   (200*1024)    // 200M
#define MAX_MEM_FREE_THRESHOLD_DEFAULT    (150*1024)    // 150M

// sdf file
#define DEFAULT_SDF_PROP "/framework/etc/{{name}}/sdf/prop.default"
#define FRAMEWORK_SDF_DIR "/framework/etc/{{name}}/sdf"
#define SERVICE_MODE_FILE "/framework/etc/{{name}}/mode.conf"

// root directory
#define FRAMEWORK_ROOT_DIR  "/framework"
#define SOTA_ROOT_DIR       "/sota_app"

namespace cppware {
namespace {{name}} {

class ConfigAarch64 : public Config {
 public:
  using ConfigAarch64Ptr = std::shared_ptr<ConfigAarch64>;
  explicit ConfigAarch64(const std::string &confile);

 protected:
  ConfigAarch64(ConfigAarch64&) = delete;
  ConfigAarch64& operator=(const ConfigAarch64&) = delete;
  void default_fill();
};

}  // namespace {{name}}
}  // namespace cppware

#endif  // {{ name|upper }}_AARCH64_CONFIG_H_
