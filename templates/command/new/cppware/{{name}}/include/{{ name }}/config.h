#ifndef {{ name|upper }}_CONFIG_H_
#define {{ name|upper }}_CONFIG_H_

#include <memory>
#include <vector>
#include <string>


namespace cppware {
namespace {{name}} {

struct ConfigProcessResource {
  uint32_t max_cpu;
  uint32_t max_mem;
  uint64_t mem_free_threshold;
};

struct ConfigGlobal {
  std::string sdf_default;
  std::string mode_file;
};

struct ConfigAppNode {
  std::string root_dir;
  std::string app_type;
  uint64_t mem_limit;
  std::vector<std::string> libs_dir;
};


class Config {
 public:
  using ConfigPtr = std::shared_ptr<Config>;

  virtual ~Config() {}
  ConfigProcessResource &get_process_resource();
  ConfigGlobal &get_global();

 protected:
  bool parse(const std::string &confile);
  virtual void default_fill() = 0;

 protected:
  ConfigProcessResource resource_;
  ConfigGlobal global_;
};

}  // namespace {{name}}
}  // namespace cppware

#endif  // {{ name|upper }}_CONFIG_H_
