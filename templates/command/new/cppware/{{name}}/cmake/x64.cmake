# cmake for platform x64

# add micro which will used in source
add_definitions(-DPLT_X64)

# add require package
find_package(jsoncpp REQUIRED)

# add executable and link library
add_executable(${MAIN_EXEC_NAME}
  src/main.cc
  src/config.cc
  src/config_factory.cc
  src/x64/config.cc
)
add_library(config_factory
  src/config.cc
  src/config_factory.cc
  src/x64/config.cc
)
target_link_libraries(
  ${MAIN_EXEC_NAME}
  ${jsoncpp_LIBS}
)

target_include_directories(${MAIN_EXEC_NAME} PUBLIC
  include
  ${jsoncpp_INCLUDES}
)

# install config file
install(
  FILES config/x64.json
  DESTINATION etc/{{name}}
  RENAME {{name}}.json
)

# install binnary
install(
  TARGETS ${MAIN_EXEC_NAME}
  DESTINATION bin
)

