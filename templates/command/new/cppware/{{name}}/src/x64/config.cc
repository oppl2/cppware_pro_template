#include <fstream>
#include "{{name}}/x64/config.h"

namespace cppware {
namespace {{name}} {

Configx64::Configx64(const std::string &confile) {
  if (!parse(confile)) {
    default_fill();
  }
}

void Configx64::default_fill() {
  // resource
  resource_.max_cpu = static_cast<uint32_t>(MAX_CPU_USED_PERPROCESS_DEFAULT);
  resource_.max_mem = static_cast<uint32_t>(MAX_MEM_USED_PERPROCESS_DEFAULT);
  resource_.mem_free_threshold = static_cast<uint32_t>(MAX_MEM_FREE_THRESHOLD_DEFAULT);
  // global
  global_.mode_file = SERVICE_MODE_FILE;
  global_.sdf_default = DEFAULT_SDF_PROP;
}

}  // namespace {{name}}
}  // namespace cppware
