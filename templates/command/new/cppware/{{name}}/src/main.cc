#include <iostream>
#include "{{name}}/config_factory.h"

int main() {
  cppware::{{name}}::ConfigFactory::instance();
  std::cout << "Hello World" << std::endl;

  return 0;
}


