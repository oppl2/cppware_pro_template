/*
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 20:46:16
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-17 22:41:40
 */
#include <fstream>
#include "{{name}}/Aarch64/config.h"

namespace cppware {
namespace {{name}} {

ConfigAarch64::ConfigAarch64(const std::string &confile) {
  if (!parse(confile)) {
    default_fill();
  }
}

void ConfigAarch64::default_fill() {
  // resource
  resource_.max_cpu = static_cast<uint32_t>(MAX_CPU_USED_PERPROCESS_DEFAULT);
  resource_.max_mem = static_cast<uint32_t>(MAX_MEM_USED_PERPROCESS_DEFAULT);
  resource_.mem_free_threshold = static_cast<uint32_t>(MAX_MEM_FREE_THRESHOLD_DEFAULT);
  // global
  global_.mode_file = SERVICE_MODE_FILE;
  global_.sdf_default = DEFAULT_SDF_PROP;
}

}  // namespace {{name}}
}  // namespace cppware
