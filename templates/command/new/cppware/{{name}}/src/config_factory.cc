/*
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 20:46:16
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-18 02:20:42
 */
#include "{{name}}/config_factory.h"
#if  defined(PLT_X64)
#include "{{name}}/x64/config.h"
#elif defined(PLT_AARCH64)
#include "{{name}}/aarch64/config.h"
#endif

namespace cppware {
namespace {{name}} {

Config::ConfigPtr ConfigFactory::instance_ = nullptr;  // PRQA S 2310

Config::ConfigPtr ConfigFactory::instance() {
  if (instance_ == nullptr)
#if  defined(PLT_X64)
  instance_ = std::shared_ptr<Config>(new Configx64(D_SRVMANAGER_CONFIG_FILE));
#elif defined(PLT_AARCH64)
  instance_ = std::shared_ptr<Config>(new ConfigAarch64(D_SRVMANAGER_CONFIG_FILE));
#else
  instance_ = nullptr;
#endif
  return instance_;
}


}  // namespace {{name}}
}  // namespace cppware


