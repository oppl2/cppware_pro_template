#include <json/json.h>
#include <string>
#include <fstream>
#include "{{name}}/config.h"

namespace cppware {
namespace {{name}} {


bool Config::parse(const std::string &confile) {
  std::ifstream pfile(confile, std::ios::in);
  if (!pfile.good()) {
    return false;
  }

  Json::Value root;
  Json::CharReaderBuilder builder;
  builder["collectComments"] = true;  // PRQA S 3050
  JSONCPP_STRING errs;

  if (!parseFromStream(builder, pfile, &root, &errs)) {
    return false;
  }

  try {
    // resource
    resource_.max_cpu = root["resource"]["max_cpu"].asUInt();
    resource_.max_mem = root["resource"]["max_mem"].asUInt();
    resource_.mem_free_threshold = root["resource"]["mem_free_threshold"].asUInt64();
    // global
    global_.mode_file = root["global"]["mode_file"].asString();
    global_.sdf_default = root["global"]["sdf_default"].asString();
  } catch (...) {
    return false;
  }

  return true;
}

ConfigProcessResource &Config::get_process_resource() {
  return resource_;
}

ConfigGlobal &Config::get_global() {
  return global_;
}

}  // namespace {{name}}
}  // namespace cppware

