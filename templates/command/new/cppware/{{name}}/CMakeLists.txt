cmake_minimum_required(VERSION 3.12)
project({{name}})

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set({{name|upper}}_RELEASE_VERSION ${% raw %}{{% endraw %}{{name|upper}}_VERSION_MAJOR{% raw %}}{% endraw %}.${% raw %}{{% endraw %}{{name|upper}}_VERSION_MINOR{% raw %}}{% endraw %}.${% raw %}{{% endraw %}{{name|upper}}_VERSION_PATCH{% raw %}}{% endraw %})
if(NOT DEFINED {{name|upper}}_VERSION_PRERELEASE)
    set({{name|upper}}_VERSION ${% raw %}{{% endraw %}{{name|upper}}_RELEASE_VERSION{% raw %}}{% endraw %})
else()
    set({{name|upper}}_VERSION ${% raw %}{{% endraw %}{{name|upper}}_RELEASE_VERSION{% raw %}}{% endraw %}-${% raw %}{{% endraw %}{{name|upper}}_VERSION_PRERELEASE{% raw %}}{% endraw %})
endif()
if(DEFINED {{name|upper}}_VERSION_BUILD)
    set({{name|upper}}_VERSION ${% raw %}{{% endraw %}{{name|upper}}_VERSION{% raw %}}{% endraw %}+${% raw %}{{% endraw %}{{name|upper}}_VERSION_BUILD{% raw %}}{% endraw %})
endif()

# set and add some build flag
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MASK_DDS_COMPILE_WARNING_FLAGS}")
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")
endif()
include_directories(include)
include(cmake/{{name|upper}}CompileOptions.cmake)

# define some variant
set(MAIN_EXEC_NAME {{name}})
set({{name|upper}}_DIR ${CMAKE_CURRENT_SOURCE_DIR})

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/x64.cmake)

# add unitest and demo
add_subdirectory(doc)
add_subdirectory(test)
#add_subdirectory(test/demo)
