#!/bin/bash

rm -rf ~/.conan/templates
cp -rf templates ~/.conan 
echo -e "\033[32m ========================================================== \033[0m"
echo -e "\033[32m Deploy conan template complete :) \033[0m"
echo -e "\033[32m ========================================================== \033[0m"